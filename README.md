# Competitive Programming Diary

---
Source code for solved programming problems.

Mental sport, nothing serious.

## Note
| Description                                                             | Link                                                                                                                   |
|-------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------|
| Significance of `ios_base::sync_with_stdio(false)` and `cie.tie(NULL)`. | [StackOverflow](https://stackoverflow.com/questions/31162367/significance-of-ios-basesync-with-stdiofalse-cin-tienull) |
| Dereference operator                                                    | [Wikipedia](https://en.wikipedia.org/wiki/Dereference_operator)                                                        |
| Digit Sum Mod                                                           | [applet-magic](http://applet-magic.com/digitsummod9.htm)                                                               |
| Unordered Set                                                           | [GeeksForGeeks](https://www.geeksforgeeks.org/unordered_set-in-cpp-stl/)                                               |

## Log

21-02-2022
- **[Codeforces Session #15](https://codeforces.com)**. Solve 427A, 1399A, 1409A.

20-02-2022

- **[Codeforces Session #14](https://codeforces.com)**. Solve 155A, 1328A.

14-02-2022

- **[Codeforces Session #13](https://codeforces.com)**. Solve 785A, 1335A.
- **[Codeforces Session #12](https://codeforces.com)**. Solve 141A, 520A, 996A.

07-02-2022

- **[Codeforces Session #11](https://codeforces.com)**. Solve 443A.

03-02-2022

- **[Codeforces Session #10](https://codeforces.com)**. Solve 144A, 148A, 469A.
- **[Codeforces Session #9](https://codeforces.com)**. Solve 61A, 200B, 228A, 705A.
- Learned about [Unordered Set](https://www.geeksforgeeks.org/unordered_set-in-cpp-stl/) to resolve 228A effectively.

01-02-2022

- **[HackerRank Interview Preparation Kit - 1 Week Preparation Kit](https://hackerrank.com)**. Cleared day 4.
- Interesting math theory at [applet-magic](http://applet-magic.com/digitsummod9.htm). Easy solution to **Recursive Digit Sum**. Depending on your approach, might have to watch out for BigInt.

31-01-2022

- **[HackerRank Interview Preparation Kit - 1 Week Preparation Kit](https://hackerrank.com)**. Cleared day 1-3.
- Note: Zig Zag Sequence is a debugging problem. It's also confusing because it seems like you can only change very specific line of code otherwise you'd still get WA even when it's correct debugging.

15-01-2022

- **[Codeforces Session #8](https://codeforces.com)**. Solve 136A, 344A, 486A, 1030A.

10-01-2022

- **[Codeforces Session #7](https://codeforces.com)**. Solve 271A, 467A.
- **[Codeforces Round #764 (Div.3)](https://codeforces.com/contests/1624)** Solve 1624A.

09-01-2022

- **[Codeforces Session #6](https://codeforces.com)**. Solve 41A, 110A, 266B, 734A.

02-01-2022

- **[Codeforces Session #5](https://codeforces.com)**. Solve 59A, 116A, 546A, 617A, 977A.
- First time learning about dereference operator and implementing loop with it in 59A.
- **[Codeforces Session #4](https://codeforces.com)**. Solve 236A, 266A, 281A, 791A.
- Valuable lesson from [StackOverflow](https://stackoverflow.com/questions/31162367/significance-of-ios-basesync-with-stdiofalse-cin-tienull) for blindly adding `ios_base::sync_with_stdio(false)` and `cie.tie(NULL)` causing unexpected output on 266A.

01-01-2022

- **[Codeforces Session #3](https://codeforces.com)**. Solve 112A, 118A, 339A.
- Commit IDE:Sublime Text build for CP environment.

31-12-2021

- **[Codeforces Session #2](https://codeforces.com)**. Solve 50A, 263A, 282A, 1623B.

28-12-2021

- **[Codeforces Session #1](https://codeforces.com)**. Solve 1A, 4A, 71A, 231A.
- **[Codeforces Round #763 (Div.2)](https://codeforces.com/contest/1623)**. Solve 1623A.
